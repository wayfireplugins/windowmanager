/**
 * Implementation of the desq-shell-unstable-v1 protocol
 */
#include <wayland-client.h>
#include <wayfire/nonstd/wlroots-full.hpp>
#include <wayfire/util/log.hpp>

#include <memory>
#include <wayfire/plugin.hpp>

#include <wayfire/output.hpp>
#include <wayfire/core.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/workspace-set.hpp>
#include <wayfire/toplevel.hpp>
#include <wayfire/toplevel-view.hpp>
#include <wayfire/signal-definitions.hpp>

#include "wayfire-toplevel-management-unstable-v1-protocol.h"
extern "C" {
#include "toplevel-management.h"
}
#include "toplevel.hpp"


class WayfireToplevelManagerProtocolImpl : public wf::plugin_interface_t {
    public:
        void init() override {
            LOGE( "==> Registering toplevel-manager" );
            tlMgr = wayfire_toplevel_manager_v1_create( wf::get_core().display );
            wf::get_core().connect( &on_view_mapped );
            wf::get_core().connect( &on_view_unmapped );
        }

        void fini() override {
        }

        bool is_unloadable() override {
            return false;
        }

    private:
        wf::signal::connection_t<wf::view_mapped_signal> on_view_mapped =
            [ = ] (wf::view_mapped_signal *ev){
                if ( auto toplevel = wf::toplevel_cast( ev->view ) ) {
                    LOGE( "    ==> Creating toplevel ", toplevel->get_title(), "  ", toplevel->get_app_id() );
                    auto handle = wayfire_toplevel_v1_create( tlMgr, toplevel->get_id() );
                    handle_for_view[ toplevel ] = std::make_unique<WayfireToplevelImpl>( toplevel, handle, &handle_for_view );
                }
            };

        wf::signal::connection_t<wf::view_unmapped_signal> on_view_unmapped =
            [ = ] (wf::view_unmapped_signal *ev){
                if ( auto toplevel = wf::toplevel_cast( ev->view ) ) {
                    LOGE( "    ==> Closing toplevel ", toplevel->get_title(), toplevel->get_app_id() );
                    handle_for_view.erase( toplevel );
                }
            };

        wayfire_toplevel_manager_v1 *tlMgr;
        ToplevelMap handle_for_view;
};

DECLARE_WAYFIRE_PLUGIN( WayfireToplevelManagerProtocolImpl );
