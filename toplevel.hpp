#include <map>
#include <wayfire/core.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/plugins/wm-actions-signals.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/window-manager.hpp>
#include <wayfire/workspace-set.hpp>

#include <wayfire/view.hpp>
#include <wayfire/toplevel.hpp>
#include <wayfire/toplevel-view.hpp>

#include <memory>
#include <wayfire/plugin.hpp>
#include <wayfire/nonstd/wlroots-full.hpp>
#include <wayfire/util/log.hpp>

#include "wayfire-toplevel-management-unstable-v1-protocol.h"
extern "C" {
#include "toplevel-management.h"
}

#pragma once

class WayfireToplevelImpl;
typedef std::map<wayfire_toplevel_view, std::unique_ptr<WayfireToplevelImpl> > ToplevelMap;

class WayfireToplevelImpl {
    public:
        WayfireToplevelImpl( wayfire_toplevel_view view, wayfire_toplevel_v1 *handle, ToplevelMap *view_to_toplevel );

        ~WayfireToplevelImpl();

    private:
        void init_request_handlers();

        /**
         * if @capture == true
         *     send view_captured event
         * else
         *     send frame_ready event
         */
        void captureView( bool capture );

        wf::signal::connection_t<wf::view_title_changed_signal> on_title_changed =
            [ = ] (auto){
                wayfire_toplevel_v1_title_changed( mHandle, mView->get_title().c_str() );
            };

        wf::signal::connection_t<wf::view_app_id_changed_signal> on_app_id_changed =
            [ = ] (auto){
                wayfire_toplevel_v1_app_id_changed( mHandle, mView->get_app_id().c_str() );
            };

        wf::signal::connection_t<wf::view_minimized_signal> on_minimized =
            [ = ] (auto){
                uint32_t newState = mHandle->state;

                /** Not minimized and set: Unset */
                if ( (not mView->minimized) && (newState & ToplevelState::Minimized) ) {
                    newState &= ~ToplevelState::Minimized;
                }

                /** Minimized and unset */
                else if ( mView->minimized && !(newState & ToplevelState::Minimized) ) {
                    newState |= ToplevelState::Minimized;
                }

                wayfire_toplevel_v1_state_changed( mHandle, newState );
            };

        wf::signal::connection_t<wf::view_fullscreen_signal> on_fullscreen =
            [ = ] (auto){
                uint32_t newState = mHandle->state;

                /** Not fullscreen and set: Unset */
                if ( (not mView->toplevel()->current().fullscreen) && (newState & ToplevelState::Fullscreen) ) {
                    newState &= ~ToplevelState::Fullscreen;
                }

                /** Fullscreen and unset */
                else if ( mView->toplevel()->current().fullscreen && !(newState & ToplevelState::Fullscreen) ) {
                    newState |= ToplevelState::Fullscreen;
                }

                wayfire_toplevel_v1_state_changed( mHandle, newState );
            };

        wf::signal::connection_t<wf::view_tiled_signal> on_tiled =
            [ = ] (auto){
                uint32_t newState = mHandle->state;

                /** Not maximized and set: Unset */
                if ( (mView->toplevel()->current().tiled_edges != wf::TILED_EDGES_ALL) && (newState & ToplevelState::Maximized) ) {
                    newState &= ~ToplevelState::Maximized;
                }

                /** Maximized and unset */
                else if ( (mView->toplevel()->current().tiled_edges == wf::TILED_EDGES_ALL) && !(newState & ToplevelState::Maximized) ) {
                    newState |= ToplevelState::Maximized;
                }

                wayfire_toplevel_v1_state_changed( mHandle, newState );
            };

        wf::signal::connection_t<wf::view_activated_state_signal> on_activated =
            [ = ] (auto){
                uint32_t newState = mHandle->state;

                /** Not active and set: Unset */
                if ( (not mView->activated) && (newState & ToplevelState::Active) ) {
                    newState &= ~ToplevelState::Active;
                }

                /** Active and unset */
                else if ( mView->activated && !(newState & ToplevelState::Active) ) {
                    newState |= ToplevelState::Active;
                }

                /** Unset DemandsAttention if active */
                if ( mView->activated && newState & ToplevelState::DemandsAttention ) {
                    newState &= ~ToplevelState::DemandsAttention;
                }

                wayfire_toplevel_v1_state_changed( mHandle, newState );
            };

        wf::signal::connection_t<wf::wm_actions_above_changed_signal> on_pinned_above =
            [ = ] (auto){
                uint32_t newState = mHandle->state;

                /** Not active and set: Unset */
                if ( (not mView->has_data( "wm-actions-above" ) ) && (newState & ToplevelState::PinnedAbove) ) {
                    newState &= ~ToplevelState::PinnedAbove;
                }

                /** Maximized and unset */
                else if ( mView->has_data( "wm-actions-above" ) && !(newState & ToplevelState::PinnedAbove) ) {
                    newState |= ToplevelState::PinnedAbove;
                }

                wayfire_toplevel_v1_state_changed( mHandle, newState );
            };

        // wf::signal::connection_t<wf::wm_actions_below_changed_signal> on_pinned_below =
        //     [ = ] (auto){
        //         uint32_t newState = mHandle->state;
        //
        //         /** Not active and set: Unset */
        //         if ( (not mView->has_data("wm-actions-below")) && (newState &
        // ToplevelState::PinnedBelow)
        // ) {
        //             newState &= ~ToplevelState::PinnedBelow;
        //         }
        //
        //         /** Maximized and unset */
        //         else if ( mView->has_data("wm-actions-below") && !(newState &
        // ToplevelState::PinnedBelow)
        // ) {
        //             newState |= ToplevelState::PinnedBelow;
        //         }
        //
        //         wayfire_toplevel_v1_state_changed( mHandle, newState );
        //     };

        wf::signal::connection_t<wf::view_set_sticky_signal> on_sticky =
            [ = ] (auto){
                uint32_t newState = mHandle->state;

                /** Not active and set: Unset */
                if ( (not mView->sticky) && (newState & ToplevelState::Sticky) ) {
                    newState &= ~ToplevelState::Sticky;
                }

                /** Maximized and unset */
                else if ( mView->sticky && !(newState & ToplevelState::Sticky) ) {
                    newState |= ToplevelState::Sticky;
                }

                wayfire_toplevel_v1_state_changed( mHandle, newState );
            };

        wf::signal::connection_t<wf::view_hints_changed_signal> on_view_hints =
            [ = ] (wf::view_hints_changed_signal *ev){
                uint32_t newState = mHandle->state;

                /** Doesn't demand attention and set: Unset */
                if ( (not ev->demands_attention) && (newState & ToplevelState::DemandsAttention) ) {
                    newState &= ~ToplevelState::DemandsAttention;
                }

                /** Demands attention and unset: Set */
                else if ( ev->demands_attention && !(newState & ToplevelState::DemandsAttention) ) {
                    newState |= ToplevelState::DemandsAttention;
                }

                LOGE( "======> ", ev->view->get_app_id(), " demands attention ", ev->demands_attention );

                wayfire_toplevel_v1_state_changed( mHandle, newState );
            };

        wf::signal::connection_t<wf::view_parent_changed_signal> on_parent_changed =
            [ = ] (auto) {
                /** Does not have a parent */
                if ( !mView->parent ) {
                    wayfire_toplevel_v1_parent_changed( mHandle, 0 );
                }

                /** Has a valid parent */
                else {
                    wayfire_toplevel_v1_parent_changed( mHandle, mView->parent->get_id() );
                }
            };

        wf::signal::connection_t<wf::view_set_output_signal> on_output_changed =
            [ = ] (wf::view_set_output_signal *ev) {
                wayfire_toplevel_v1_output_changed( mHandle, ev->output->get_id() );
            };

        wf::signal::connection_t<wf::view_moved_to_wset_signal> on_wset_changed =
            [ = ] (wf::view_moved_to_wset_signal *ev) {
                if ( ev->view != mView ) {
                    return;
                }

                uint32_t old_idx = (ev->old_wset != nullptr ? ev->old_wset->get_index() : 0);
                uint32_t new_idx = (ev->new_wset != nullptr ? ev->new_wset->get_index() : 0);

                wayfire_toplevel_v1_workspace_set_changed( mHandle, new_idx, old_idx );
            };

        wf::signal::connection_t<wf::view_change_workspace_signal> on_workspace_changed =
            [ = ] (wf::view_change_workspace_signal *ev) {
                if ( ev->view != mView ) {
                    return;
                }

                wf::point_t new_ws = ev->to;
                wf::point_t old_ws = {0, 0};

                if ( ev->old_workspace_valid ) {
                    old_ws = ev->from;
                }

                wayfire_toplevel_v1_workspace_changed( mHandle, new_ws.x, new_ws.y, old_ws.x, old_ws.y );
            };

        wayfire_toplevel_view mView;
        wayfire_toplevel_v1 *mHandle;
        ToplevelMap *mViewToToplevel;

        wf::wl_timer<true> previewTimer;

        wf::wl_listener_wrapper toplevel_focus_request;
        wf::wl_listener_wrapper toplevel_maximize_request;
        wf::wl_listener_wrapper toplevel_minimize_request;
        wf::wl_listener_wrapper toplevel_restore_request;
        wf::wl_listener_wrapper toplevel_fullscreen_request;
        wf::wl_listener_wrapper toplevel_set_pin_above_request;
        wf::wl_listener_wrapper toplevel_set_pin_below_request;
        wf::wl_listener_wrapper toplevel_set_sticky_request;
        wf::wl_listener_wrapper toplevel_set_minimized_geometry_request;
        wf::wl_listener_wrapper toplevel_pid_request;
        wf::wl_listener_wrapper toplevel_close_request;
        wf::wl_listener_wrapper toplevel_kill_request;
};
