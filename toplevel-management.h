/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2017, 2018 Drew DeVault
 * Copyright (c) 2014 Jari Vetoniemi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-client.h>
#include <wayland-server-core.h>

#include "wayfire-toplevel-management-unstable-v1-protocol.h"

#pragma once

struct wayfire_toplevel_v1;
struct wayfire_toplevel_manager_v1;

enum ToplevelState {
    Active           = 1 << 0,
    Minimized        = 1 << 1,
    Maximized        = 1 << 2,
    Fullscreen       = 1 << 3,
    PinnedAbove      = 1 << 4,
    PinnedBelow      = 1 << 5,
    Sticky           = 1 << 6,
    DemandsAttention = 1 << 7,
};

struct wayfire_toplevel_v1 {
    struct wayfire_toplevel_manager_v1 *manager;
    struct wl_list                     resources;
    struct wl_list                     link;
    struct wl_event_source             *idle_source;

    char                               *title, *oldTitle;
    char                               *appId, *oldAppId;
    uint32_t                           parent, oldParent;
    uint32_t                           state, oldState;

    uint32_t                           uuid;
    bool                               captureDisabled;

    struct {
        struct wl_signal request_focus;
        struct wl_signal request_maximize;
        struct wl_signal request_minimize;
        struct wl_signal request_restore;
        struct wl_signal request_fullscreen;
        struct wl_signal request_set_pin_above;
        struct wl_signal request_set_pin_below;
        struct wl_signal request_set_sticky;
        struct wl_signal request_set_minimized_geometry;
        struct wl_signal request_pid;
        struct wl_signal request_close;
        struct wl_signal request_kill;
        struct wl_signal destroy;
    }
                                       events;

    void                               *data;
};

struct wayfire_toplevel_v1_fullscreen_event {
    struct wayfire_toplevel_v1 *toplevel;
    bool                       state;
    struct wlr_output          *output;
};

struct wayfire_toplevel_v1_set_pin_above_event {
    struct wayfire_toplevel_v1 *toplevel;
    bool                       above;
};

struct wayfire_toplevel_v1_set_pin_below_event {
    struct wayfire_toplevel_v1 *toplevel;
    bool                       below;
};

struct wayfire_toplevel_v1_set_sticky_event {
    struct wayfire_toplevel_v1 *toplevel;
    bool                       sticky;
};

struct wayfire_toplevel_v1_set_minimized_geometry_event {
    struct wayfire_toplevel_v1 *toplevel;
    struct wlr_surface         *surface;
    int32_t                    x, y, width, height;
};


void wayfire_toplevel_v1_title_changed( struct wayfire_toplevel_v1 *toplevel, const char *title );
void wayfire_toplevel_v1_app_id_changed( struct wayfire_toplevel_v1 *toplevel, const char *appId );
void wayfire_toplevel_v1_state_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t state );
void wayfire_toplevel_v1_parent_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t parent_uuid );
void wayfire_toplevel_v1_geometry_changed( struct wayfire_toplevel_v1 *toplevel, int x, int y, int w, int h );
void wayfire_toplevel_v1_process_info( struct wayfire_toplevel_v1 *toplevel, uint32_t pid, uint32_t uid, uint32_t gid );
void wayfire_toplevel_v1_output_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t new_output );
void wayfire_toplevel_v1_workspace_set_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t new_ws, uint32_t old_ws );
void wayfire_toplevel_v1_workspace_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t new_x, uint32_t new_y, uint32_t old_x, uint32_t old_y );
void wayfire_toplevel_v1_destroy( struct wayfire_toplevel_v1 *toplevel );

struct wayfire_toplevel_manager_v1 {
    struct wl_event_loop *event_loop;
    struct wl_global     *global;
    struct wl_list       resources; // wl_resource_get_link()
    struct wl_list       toplevels; // wayfire_toplevel_v1.link

    struct wl_listener   display_destroy;

    struct {
        struct wl_signal request_get_toplevel_for_uuid;
        struct wl_signal request_toplevels;
        struct wl_signal request_capture_toplevel;
        struct wl_signal request_disable_toplevel_capture;
        struct wl_signal destroy;
    }
                         events;

    void                 *data;
};


struct wayfire_toplevel_manager_v1 *wayfire_toplevel_manager_v1_create( struct wl_display *display );

struct wayfire_toplevel_v1 *wayfire_toplevel_v1_create( struct wayfire_toplevel_manager_v1 *manager, uint32_t uuid );

/** Send a toplevel event */
void wayfire_toplevel_manager_v1_toplevel( struct wayfire_toplevel_manager_v1 *manager, struct wayfire_toplevel_v1 *toplevel );
