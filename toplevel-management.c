/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2017, 2018 Drew DeVault
 * Copyright (c) 2014 Jari Vetoniemi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#define _POSIX_C_SOURCE    200809L

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/util/log.h>
#include "wayfire-toplevel-management-unstable-v1-protocol.h"

#include "toplevel-management.h"

#define WAYFIRE_TOPLEVEL_MANAGEMENT_V1_VERSION    1


static const struct zwayfire_toplevel_v1_interface wayfire_toplevel_impl;

static struct wayfire_toplevel_v1 *toplevel_handle_from_resource( struct wl_resource *resource ) {
    assert(
        wl_resource_instance_of(
            resource,
            &zwayfire_toplevel_v1_interface,
            &wayfire_toplevel_impl
        )
    );

    return wl_resource_get_user_data( resource );
}


static void handle_wayfire_toplevel_request_focus( struct wl_client *, struct wl_resource *resource ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        wl_signal_emit_mutable( &toplevel->events.request_focus, toplevel );
    }
}


static void handle_wayfire_toplevel_request_maximize( struct wl_client *, struct wl_resource *resource ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        wl_signal_emit_mutable( &toplevel->events.request_maximize, toplevel );
    }
}


static void handle_wayfire_toplevel_request_minimize( struct wl_client *, struct wl_resource *resource ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        wl_signal_emit_mutable( &toplevel->events.request_minimize, toplevel );
    }
}


static void handle_wayfire_toplevel_request_restore( struct wl_client *, struct wl_resource *resource ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        wl_signal_emit_mutable( &toplevel->events.request_restore, toplevel );
    }
}


static void handle_wayfire_toplevel_request_fullscreen( struct wl_client *, struct wl_resource *resource, uint32_t state, struct wl_resource *output ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        struct wlr_output *wlOutput = NULL;

        if ( output && state ) {
            wlOutput = wlr_output_from_resource( output );
        }

        struct wayfire_toplevel_v1_fullscreen_event event =
        {
            .toplevel = toplevel,
            .state    = state,
            .output   = wlOutput,
        };

        wl_signal_emit_mutable( &toplevel->events.request_fullscreen, &event );
    }
}


static void handle_wayfire_toplevel_request_set_pin_above( struct wl_client *, struct wl_resource *resource, int32_t on_top ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        struct wayfire_toplevel_v1_set_pin_above_event event =
        {
            .toplevel = toplevel,
            .above    = on_top,
        };

        wl_signal_emit_mutable( &toplevel->events.request_set_pin_above, &event );
    }
}


static void handle_wayfire_toplevel_request_set_pin_below( struct wl_client *, struct wl_resource *resource, int32_t on_bottom ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        struct wayfire_toplevel_v1_set_pin_below_event event =
        {
            .toplevel = toplevel,
            .below    = on_bottom,
        };

        wl_signal_emit_mutable( &toplevel->events.request_set_pin_below, &event );
    }
}


static void handle_wayfire_toplevel_request_set_sticky( struct wl_client *, struct wl_resource *resource, int32_t sticky ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        struct wayfire_toplevel_v1_set_sticky_event event =
        {
            .toplevel = toplevel,
            .sticky   = sticky,
        };

        wl_signal_emit_mutable( &toplevel->events.request_set_sticky, &event );
    }
}


static void handle_wayfire_toplevel_request_set_minimized_geometry( struct wl_client *, struct wl_resource *resource, struct wl_resource *surf,
                                                                    int32_t x, int32_t y, int32_t w, int32_t h ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        if ( (w * h) < 0 ) {
            wl_resource_post_error(
                resource,
                0,
                "invalid rectangle passed to set_rectangle: width/height < 0"
            );

            return;
        }

        struct wayfire_toplevel_v1_set_minimized_geometry_event event =
        {
            .toplevel = toplevel,
            .surface  = (struct wlr_surface *)surf,
            .x        = x,
            .y        = y,
            .width    = w,
            .height   = h,
        };

        wl_signal_emit_mutable( &toplevel->events.request_set_minimized_geometry, &event );
    }
}


static void handle_wayfire_toplevel_request_pid( struct wl_client *, struct wl_resource *resource ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        wl_signal_emit_mutable( &toplevel->events.request_pid, toplevel );
    }
}


static void handle_wayfire_toplevel_request_close( struct wl_client *, struct wl_resource *resource ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        wl_signal_emit_mutable( &toplevel->events.request_close, toplevel );
    }
}


static void handle_wayfire_toplevel_request_kill( struct wl_client *, struct wl_resource *resource ) {
    struct wayfire_toplevel_v1 *toplevel = toplevel_handle_from_resource( resource );

    if ( toplevel ) {
        wl_signal_emit_mutable( &toplevel->events.request_kill, toplevel );
    }
}


static void handle_wayfire_toplevel_destroy( struct wl_client *, struct wl_resource *resource ) {
    wl_resource_destroy( resource );
}


static const struct zwayfire_toplevel_v1_interface wayfire_toplevel_impl =
{
    .focus                  = handle_wayfire_toplevel_request_focus,
    .maximize               = handle_wayfire_toplevel_request_maximize,
    .minimize               = handle_wayfire_toplevel_request_minimize,
    .restore                = handle_wayfire_toplevel_request_restore,
    .fullscreen             = handle_wayfire_toplevel_request_fullscreen,
    .set_pin_above          = handle_wayfire_toplevel_request_set_pin_above,
    .set_pin_below          = handle_wayfire_toplevel_request_set_pin_below,
    .set_sticky             = handle_wayfire_toplevel_request_set_sticky,
    .set_minimized_geometry = handle_wayfire_toplevel_request_set_minimized_geometry,
    .get_process_info       = handle_wayfire_toplevel_request_pid,
    .close                  = handle_wayfire_toplevel_request_close,
    .kill    = handle_wayfire_toplevel_request_kill,
    .destroy = handle_wayfire_toplevel_destroy,
};


static void toplevel_idle_send_done( void *data ) {
    struct wayfire_toplevel_v1 *toplevel = data;
    struct wl_resource         *resource;

    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_done( resource );
    }

    toplevel->idle_source = NULL;
}


static void toplevel_update_idle_source( struct wayfire_toplevel_v1 *toplevel ) {
    if ( toplevel->idle_source ) {
        return;
    }

    toplevel->idle_source = wl_event_loop_add_idle( toplevel->manager->event_loop, toplevel_idle_send_done, toplevel );
}


void wayfire_toplevel_v1_title_changed( struct wayfire_toplevel_v1 *toplevel, const char *title ) {
    /** Copy the current title to old title */
    free( toplevel->oldTitle );

    if ( toplevel->title != NULL ) {
        toplevel->oldTitle = strdup( toplevel->title );
    }

    else {
        toplevel->oldTitle = strdup( " " );
    }

    /** Copy the incoming title to title */
    free( toplevel->title );
    toplevel->title = strdup( title );

    if ( toplevel->title == NULL ) {
        return;
    }

    struct wl_resource *resource;
    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_title_changed( resource, toplevel->oldTitle, toplevel->title );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_app_id_changed( struct wayfire_toplevel_v1 *toplevel, const char *appId ) {
    /** Copy the current title to old title */
    free( toplevel->oldAppId );

    if ( toplevel->appId != NULL ) {
        toplevel->oldAppId = strdup( toplevel->appId );
    }
    else {
        toplevel->oldAppId = strdup( " " );
    }

    /** Copy the incoming title to title */
    free( toplevel->appId );
    toplevel->appId = strdup( appId );

    if ( toplevel->appId == NULL ) {
        // LOGE( "failed to allocate memory for toplevel appId" );
        return;
    }

    struct wl_resource *resource;
    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_app_id_changed( resource, toplevel->oldAppId, toplevel->appId );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_state_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t state ) {
    /** Copy the current title to old title */
    toplevel->oldState = toplevel->state;

    /** Copy the incoming title to title */
    toplevel->state = state;

    struct wl_resource *resource;
    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_state_changed( resource, toplevel->oldState, toplevel->state );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_parent_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t parent_uuid ) {
    if ( parent_uuid == toplevel->parent ) {
        /* only send parent event to the clients if there was a change */
        return;
    }

    toplevel->oldParent = toplevel->parent;
    toplevel->parent    = parent_uuid;

    struct wl_resource *toplevel_resource, *tmp;
    wl_resource_for_each_safe( toplevel_resource, tmp, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_parent_changed( toplevel_resource, toplevel->oldParent, toplevel->parent );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_geometry_changed( struct wayfire_toplevel_v1 *toplevel, int x, int y, int w, int h ) {
    struct wl_resource *resource;

    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_geometry_changed( resource, x, y, w, h );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_process_info( struct wayfire_toplevel_v1 *toplevel, uint32_t pid, uint32_t uid, uint32_t gid ) {
    struct wl_resource *resource;

    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_process_info( resource, pid, uid, gid );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_output_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t new_output ) {
    struct wl_resource *resource;

    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_output_changed( resource, new_output );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_workspace_set_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t new_ws, uint32_t old_ws ) {
    struct wl_resource *resource;

    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_workspace_set_changed( resource, new_ws, old_ws );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_workspace_changed( struct wayfire_toplevel_v1 *toplevel, uint32_t new_x, uint32_t new_y, uint32_t old_x, uint32_t old_y ) {
    struct wl_resource *resource;

    wl_resource_for_each( resource, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_workspace_changed( resource, new_x, new_y, old_x, old_y );
    }

    toplevel_update_idle_source( toplevel );
}


void wayfire_toplevel_v1_destroy( struct wayfire_toplevel_v1 *toplevel ) {
    if ( !toplevel ) {
        return;
    }

    wl_signal_emit_mutable( &toplevel->events.destroy, toplevel );

    struct wl_resource *resource, *tmp;
    wl_resource_for_each_safe( resource, tmp, &toplevel->resources ) {
        zwayfire_toplevel_v1_send_closed( resource );
        wl_resource_set_user_data( resource, NULL );
        wl_list_remove( wl_resource_get_link( resource ) );
        wl_list_init( wl_resource_get_link( resource ) );
    }

    if ( toplevel->idle_source ) {
        wl_event_source_remove( toplevel->idle_source );
    }

    wl_list_remove( &toplevel->link );

    free( toplevel->oldTitle );
    free( toplevel->title );
    free( toplevel->oldAppId );
    free( toplevel->appId );
    free( toplevel );
}


static const struct zwayfire_toplevel_manager_v1_interface wayfire_toplevel_manager_impl;

static struct wayfire_toplevel_manager_v1 *toplevel_manager_from_resource( struct wl_resource *resource ) {
    assert(
        wl_resource_instance_of(
            resource,
            &zwayfire_toplevel_manager_v1_interface,
            &wayfire_toplevel_manager_impl
        )
    );

    return wl_resource_get_user_data( resource );
}


static void wayfire_toplevel_resource_destroy( struct wl_resource *resource ) {
    wl_list_remove( wl_resource_get_link( resource ) );
}


static struct wl_resource *create_toplevel_resource_for_resource( struct wayfire_toplevel_v1 *toplevel, struct wl_resource *manager_resource ) {
    struct wl_client   *client   = wl_resource_get_client( manager_resource );
    struct wl_resource *resource = wl_resource_create(
        client,
        &zwayfire_toplevel_v1_interface,
        wl_resource_get_version( manager_resource ),
        0
    );

    if ( !resource ) {
        wl_client_post_no_memory( client );
        return NULL;
    }

    wl_resource_set_implementation( resource, &wayfire_toplevel_impl, toplevel, wayfire_toplevel_resource_destroy );

    wl_list_insert( &toplevel->resources, wl_resource_get_link( resource ) );

    printf( "  ===> create_toplevel_resource_for_resource.send_toplevel %d\n", toplevel->uuid );
    zwayfire_toplevel_manager_v1_send_new_toplevel( manager_resource, resource, toplevel->uuid );

    return resource;
}


struct wayfire_toplevel_v1 *wayfire_toplevel_v1_create( struct wayfire_toplevel_manager_v1 *manager, uint32_t uuid ) {
    struct wayfire_toplevel_v1 *toplevel = calloc( 1, sizeof(struct wayfire_toplevel_v1) );

    if ( !toplevel ) {
        return NULL;
    }

    /** Store the uuid */
    toplevel->uuid = uuid;

    wl_list_insert( &manager->toplevels, &toplevel->link );
    toplevel->manager = manager;

    wl_list_init( &toplevel->resources );

    wl_signal_init( &toplevel->events.request_focus );
    wl_signal_init( &toplevel->events.request_maximize );
    wl_signal_init( &toplevel->events.request_minimize );
    wl_signal_init( &toplevel->events.request_restore );
    wl_signal_init( &toplevel->events.request_fullscreen );
    wl_signal_init( &toplevel->events.request_set_pin_above );
    wl_signal_init( &toplevel->events.request_set_pin_below );
    wl_signal_init( &toplevel->events.request_set_sticky );
    wl_signal_init( &toplevel->events.request_set_minimized_geometry );
    wl_signal_init( &toplevel->events.request_pid );
    wl_signal_init( &toplevel->events.request_close );
    wl_signal_init( &toplevel->events.request_kill );
    wl_signal_init( &toplevel->events.destroy );

    struct wl_resource *manager_resource, *tmp;

    wl_resource_for_each_safe( manager_resource, tmp, &manager->resources ) {
        create_toplevel_resource_for_resource( toplevel, manager_resource );
    }

    return toplevel;
}


static void toplevel_send_details_to_toplevel_resource( struct wayfire_toplevel_v1 *toplevel, struct wl_resource *resource ) {
    if ( toplevel->title ) {
        zwayfire_toplevel_v1_send_title_changed( resource, toplevel->oldTitle, toplevel->title );
    }

    if ( toplevel->appId ) {
        zwayfire_toplevel_v1_send_app_id_changed( resource, toplevel->oldAppId, toplevel->appId );
    }

    zwayfire_toplevel_v1_send_state_changed( resource, toplevel->oldState, toplevel->state );

    zwayfire_toplevel_v1_send_parent_changed( resource, toplevel->oldParent, toplevel->parent );

    zwayfire_toplevel_v1_send_done( resource );
}


static void handle_wayfire_toplevel_manager_request_get_toplevel_for_uuid( struct wl_client *client, struct wl_resource *resource, uint32_t uuid ) {
    struct wayfire_toplevel_manager_v1 *manager = toplevel_manager_from_resource( resource );

    if ( manager ) {
        struct wayfire_toplevel_v1 *toplevel, *tmp;

        printf( "  =====> Sending toplevel for uuid %d\n", uuid );
        wl_list_for_each_safe( toplevel, tmp, &manager->toplevels, link ) {
            if ( toplevel->uuid == uuid ) {
                struct wl_resource *toplevel_resource = wl_resource_find_for_client( &toplevel->resources, client );

                /** Send the toplevel() signal */
                if ( toplevel_resource ) {
                    printf( "  ===> zwayfire_toplevel_manager_v1_send_toplevel %p, %d\n", (void *)toplevel_resource, toplevel->uuid );
                    zwayfire_toplevel_manager_v1_send_toplevel( resource, toplevel_resource, toplevel->uuid );
                }

                /** Resource was not found: Create it! (event is automatically sent) */
                else {
                    toplevel_resource = create_toplevel_resource_for_resource( toplevel, resource );
                }

                /** We're done, thank you. */
                break;
            }
        }

        wl_list_for_each_safe( toplevel, tmp, &manager->toplevels, link ) {
            /** We will surely have the resource for this toplevel */
            struct wl_resource *toplevel_resource = wl_resource_find_for_client( &toplevel->resources, client );

            /** Send the toplevel events */
            toplevel_send_details_to_toplevel_resource( toplevel, toplevel_resource );
        }
    }
}


static void handle_wayfire_toplevel_manager_request_toplevels( struct wl_client *client, struct wl_resource *resource ) {
    struct wayfire_toplevel_manager_v1 *manager = toplevel_manager_from_resource( resource );

    if ( manager ) {
        struct wayfire_toplevel_v1 *toplevel, *tmp;

        printf( "  =====> SENDING ALL TOPLEVELS (USER REQUEST)\n" );
        wl_list_for_each_safe( toplevel, tmp, &manager->toplevels, link ) {
            struct wl_resource *toplevel_resource = wl_resource_find_for_client( &toplevel->resources, client );

            /** Send the toplevel() signal */
            if ( toplevel_resource ) {
                printf( "  ==> Sending existing toplevel %p, %d\n", (void *)toplevel_resource, toplevel->uuid );
                zwayfire_toplevel_manager_v1_send_toplevel( resource, toplevel_resource, toplevel->uuid );
            }

            /** Resource was not found: Create it! (event is automatically sent) */
            else {
                toplevel_resource = create_toplevel_resource_for_resource( toplevel, resource );
            }
        }

        /** Done sending the toplevel info */
        zwayfire_toplevel_manager_v1_send_done( resource );

        wl_list_for_each_safe( toplevel, tmp, &manager->toplevels, link ) {
            /** We will surely have the resource for this toplevel */
            struct wl_resource *toplevel_resource = wl_resource_find_for_client( &toplevel->resources, client );

            /** Send the toplevel events */
            printf( "  ==> Sending toplevel details %p, %d\n", (void *)toplevel_resource, toplevel->uuid );
            toplevel_send_details_to_toplevel_resource( toplevel, toplevel_resource );
        }

        printf( "  ==================================  \n\n" );
    }
}


static void handle_wayfire_toplevel_manager_request_disable_toplevel_capture( struct wl_client *, struct wl_resource *resource, uint32_t uuid, uint32_t state ) {
    struct wayfire_toplevel_manager_v1 *manager = toplevel_manager_from_resource( resource );

    if ( manager ) {
        struct wayfire_toplevel_v1 *toplevel, *tmp;

        printf( "  =====> Sending toplevel for uuid %d\n", uuid );
        wl_list_for_each_safe( toplevel, tmp, &manager->toplevels, link ) {
            if ( toplevel->uuid == uuid ) {
                toplevel->captureDisabled = (bool)state;
                break;
            }
        }
    }
}


static void wayfire_toplevel_manager_handle_stop( struct wl_client *, struct wl_resource *resource ) {
    assert(
        wl_resource_instance_of(
            resource,
            &zwayfire_toplevel_manager_v1_interface,
            &wayfire_toplevel_manager_impl
        )
    );

    zwayfire_toplevel_manager_v1_send_finished( resource );
    wl_resource_destroy( resource );
}


static const struct zwayfire_toplevel_manager_v1_interface wayfire_toplevel_manager_impl =
{
    .get_toplevel_for_uuid    = handle_wayfire_toplevel_manager_request_get_toplevel_for_uuid,
    .toplevels                = handle_wayfire_toplevel_manager_request_toplevels,
    .disable_toplevel_capture = handle_wayfire_toplevel_manager_request_disable_toplevel_capture,
    .stop                     = wayfire_toplevel_manager_handle_stop,
};

static void wayfire_toplevel_manager_resource_destroy( struct wl_resource *resource ) {
    wl_list_remove( wl_resource_get_link( resource ) );
}


static void wayfire_toplevel_manager_bind( struct wl_client *client, void *data, uint32_t version, uint32_t id ) {
    struct wayfire_toplevel_manager_v1 *manager  = data;
    struct wl_resource                 *resource = wl_resource_create( client, &zwayfire_toplevel_manager_v1_interface, version, id );

    if ( !resource ) {
        wl_client_post_no_memory( client );
        return;
    }

    wl_resource_set_implementation( resource, &wayfire_toplevel_manager_impl, manager, wayfire_toplevel_manager_resource_destroy );
    wl_list_insert( &manager->resources, wl_resource_get_link( resource ) );

    struct wayfire_toplevel_v1 *toplevel, *tmp;

    /**
     * First loop: create a handle for all toplevels for all clients.
     * Separation into two loops avoid the case where a child handle
     * is created before a parent handle, so the parent relationship
     * could not be sent to a client.
     */
    printf( "  =====> SENDING ALL TOPLEVELS\n" );
    wl_list_for_each_safe( toplevel, tmp, &manager->toplevels, link ) {
        printf( "  ====> Creating toplevel resource for %d\n", toplevel->uuid );
        create_toplevel_resource_for_resource( toplevel, resource );
    }

    /** Second loop: send details about each toplevel. */
    wl_list_for_each_safe( toplevel, tmp, &manager->toplevels, link ) {
        struct wl_resource *toplevel_resource = wl_resource_find_for_client( &toplevel->resources, client );

        toplevel_send_details_to_toplevel_resource( toplevel, toplevel_resource );
    }

    /** Send the "done" event, so that the clients can prepare to commit the changes */
    zwayfire_toplevel_manager_v1_send_done( resource );
}


static void handle_display_destroy( struct wl_listener *listener, void * ) {
    struct wayfire_toplevel_manager_v1 *manager = wl_container_of( listener, manager, display_destroy );

    wl_signal_emit_mutable( &manager->events.destroy, manager );
    wl_list_remove( &manager->display_destroy.link );
    wl_global_destroy( manager->global );
    free( manager );
}


struct wayfire_toplevel_manager_v1 *wayfire_toplevel_manager_v1_create( struct wl_display *display ) {
    struct wayfire_toplevel_manager_v1 *manager = calloc( 1, sizeof(struct wayfire_toplevel_manager_v1) );

    if ( !manager ) {
        return NULL;
    }

    manager->event_loop = wl_display_get_event_loop( display );
    manager->global     = wl_global_create(
        display,
        &zwayfire_toplevel_manager_v1_interface,
        WAYFIRE_TOPLEVEL_MANAGEMENT_V1_VERSION,
        manager,
        wayfire_toplevel_manager_bind
    );

    if ( !manager->global ) {
        free( manager );
        return NULL;
    }

    wl_signal_init( &manager->events.request_get_toplevel_for_uuid );
    wl_signal_init( &manager->events.request_toplevels );
    wl_signal_init( &manager->events.request_capture_toplevel );
    wl_signal_init( &manager->events.request_disable_toplevel_capture );
    wl_signal_init( &manager->events.destroy );

    wl_list_init( &manager->resources );
    wl_list_init( &manager->toplevels );

    manager->display_destroy.notify = handle_display_destroy;
    wl_display_add_destroy_listener( display, &manager->display_destroy );

    return manager;
}


static void manager_resource_send_toplevel( struct wl_resource *manager, struct wayfire_toplevel_v1 *toplevel ) {
    struct wl_client   *client            = wl_resource_get_client( manager );
    struct wl_resource *toplevel_resource = wl_resource_find_for_client( &toplevel->resources, client );

    printf( "  ===> manager_resource_send_toplevel.send_toplevel %d\n", toplevel->uuid );
    zwayfire_toplevel_manager_v1_send_toplevel( manager, toplevel_resource, toplevel->uuid );
    toplevel_send_details_to_toplevel_resource( toplevel, toplevel_resource );
}


void wayfire_toplevel_manager_v1_toplevel( struct wayfire_toplevel_manager_v1 *manager, struct wayfire_toplevel_v1 *toplevel ) {
    struct wl_resource *manager_resource, *tmp;

    wl_resource_for_each_safe( manager_resource, tmp, &manager->resources ) {
        manager_resource_send_toplevel( manager_resource, toplevel );
    }
}
