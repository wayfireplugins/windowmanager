#include <memory>
#include <fcntl.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <wayfire/core.hpp>
#include <wayfire/window-manager.hpp>
#include <wayfire/nonstd/wlroots-full.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/plugin.hpp>
#include <wayfire/plugins/wm-actions-signals.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/util/log.hpp>
#include <wayfire/view.hpp>

#include "toplevel.hpp"

WayfireToplevelImpl::WayfireToplevelImpl( wayfire_toplevel_view view, wayfire_toplevel_v1 *handle, ToplevelMap *view_to_toplevel ) {
    this->mView           = view;
    this->mHandle         = handle;
    this->mViewToToplevel = view_to_toplevel;

    init_request_handlers();

    toplevel_focus_request.connect( &handle->events.request_focus );
    toplevel_maximize_request.connect( &handle->events.request_maximize );
    toplevel_minimize_request.connect( &handle->events.request_minimize );
    toplevel_restore_request.connect( &handle->events.request_restore );
    toplevel_fullscreen_request.connect( &handle->events.request_fullscreen );
    toplevel_set_pin_above_request.connect( &handle->events.request_set_pin_above );
    toplevel_set_pin_below_request.connect( &handle->events.request_set_pin_below );
    toplevel_set_sticky_request.connect( &handle->events.request_set_sticky );
    toplevel_set_minimized_geometry_request.connect( &handle->events.request_set_minimized_geometry );
    toplevel_pid_request.connect( &handle->events.request_pid );
    toplevel_close_request.connect( &handle->events.request_close );
    toplevel_kill_request.connect( &handle->events.request_kill );

    wayfire_toplevel_v1_title_changed( mHandle, mView->get_title().c_str() );
    wayfire_toplevel_v1_app_id_changed( mHandle, mView->get_app_id().c_str() );

    /** Retrieve states */
    uint32_t state = 0;

    // active
    if ( mView->activated ) {
        state |= ToplevelState::Active;
    }

    // minimized
    if ( mView->minimized ) {
        state |= ToplevelState::Minimized;
    }

    // maximized
    if ( mView->toplevel()->current().tiled_edges == wf::TILED_EDGES_ALL ) {
        state |= ToplevelState::Maximized;
    }

    // fullscreen
    if ( mView->toplevel()->current().fullscreen ) {
        state |= ToplevelState::Fullscreen;
    }

    // pin_above
    if ( mView->has_data( "wm-actions-above" ) ) {
        state |= ToplevelState::PinnedAbove;
    }

    // pin_below
    if ( mView->has_data( "wm-actions-below" ) ) {
        state |= ToplevelState::PinnedBelow;
    }

    // sticky
    if ( mView->sticky ) {
        state |= ToplevelState::Sticky;
    }

    // demands_attention
    if ( mView->has_data( "demands-attention" ) ) {
        state |= ToplevelState::DemandsAttention;
    }

    /** Send the states */
    wayfire_toplevel_v1_state_changed( mHandle, state );

    mView->connect( &on_title_changed );
    mView->connect( &on_app_id_changed );
    mView->connect( &on_minimized );
    mView->connect( &on_fullscreen );
    mView->connect( &on_tiled );
    mView->connect( &on_activated );
    mView->connect( &on_pinned_above );
    // mView->connect( &on_pinned_below );
    mView->connect( &on_sticky );
    mView->connect( &on_view_hints );
    mView->connect( &on_parent_changed );
    mView->connect( &on_output_changed );
}


WayfireToplevelImpl::~WayfireToplevelImpl() {
    wayfire_toplevel_v1_destroy( mHandle );
}


void WayfireToplevelImpl::init_request_handlers() {
    toplevel_focus_request.set_callback(
        [ & ] ( auto ) {
            wf::get_core().default_wm->focus_request( mView );
        }
    );

    toplevel_maximize_request.set_callback(
        [ & ] ( auto ) {
            wf::get_core().default_wm->tile_request( mView, wf::TILED_EDGES_ALL );
        }
    );

    toplevel_minimize_request.set_callback(
        [ & ] ( auto ) {
            wf::get_core().default_wm->minimize_request( mView, true );
        }
    );

    toplevel_restore_request.set_callback(
        [ & ] ( auto ) {
            /** Ensure that this view is not fullscreened */
            if ( mView->toplevel()->current().fullscreen ) {
            }

            /** This view was minimized */
            if ( mView->minimized ) {
                /** Unminimize */
                wf::get_core().default_wm->minimize_request( mView, false );
            }

            /** This view was tiled (i.e., maximized, or other tile) */
            else if ( mView->toplevel()->current().tiled_edges ) {
                /** Unmaximize */
                wf::get_core().default_wm->tile_request( mView, false );
            }
        }
    );

    toplevel_fullscreen_request.set_callback(
        [ & ] ( void *data ) {
            auto ev = static_cast<wayfire_toplevel_v1_fullscreen_event *>(data);
            auto wo = wf::get_core().output_layout->find_output( ev->output );

            wf::get_core().default_wm->fullscreen_request( mView, wo, ev->state );
        }
    );

    toplevel_set_pin_above_request.set_callback(
        [ & ] ( void *data ) {
            auto ev = static_cast<wayfire_toplevel_v1_set_pin_above_event *>(data);
            wf::wm_actions_set_above_state_signal set_on_top;
            set_on_top.view  = mView;
            set_on_top.above = ev->above;

            if ( mView->get_output() ) {
                mView->get_output()->emit( &set_on_top );
            }
        }
    );

    // toplevel_set_pin_below_request.set_callback(
    //     [ & ] ( void *data ) {
    //         auto ev = static_cast<wayfire_toplevel_v1_set_pin_above_event *>( data );
    //         wf::wm_actions_set_above_state_signal set_on_top;
    //         set_on_top.view  = mView;
    //         set_on_top.above = ev->above;
    //
    //         if ( mView->output ) {
    //             mView->output->emit( &set_on_top );
    //         }
    //     }
    // );

    toplevel_set_sticky_request.set_callback(
        [ & ] ( void *data ) {
            auto ev = static_cast<wayfire_toplevel_v1_set_sticky_event *>(data);
            mView->set_sticky( ev->sticky );
        }
    );

    toplevel_set_minimized_geometry_request.set_callback(
        [ & ] ( void *data ) {
            auto ev      = static_cast<wayfire_toplevel_v1_set_minimized_geometry_event *>(data);
            auto surface = wf::wl_surface_to_wayfire_view( ev->surface->resource );

            if ( !surface ) {
                LOGE( "Setting minimize hint to unknown surface. Wayfire currently"
                      "supports only setting hints relative to views." );
            }

            if ( surface->get_output() != mView->get_output() ) {
                LOGE( "Minimize hint set to surface on a different output, problems might arise." );
                /* TODO: translate coordinates in case minimize hint is on another output */
            }

            wlr_box hint { ev->x, ev->y, ev->width, ev->height };
            wf::pointf_t relative = surface->get_surface_root_node()->to_global( { 0, 0 } );
            hint.x += relative.x;
            hint.y += relative.y;
            mView->set_minimize_hint( hint );
        }
    );

    toplevel_pid_request.set_callback(
        [ & ] ( auto ) {
            pid_t pid = 0;
            uid_t uid = 0;
            gid_t gid = 0;

            /** Get the view credentials */
            wl_client_get_credentials( mView->get_client(), &pid, &uid, &gid );

            /** If we have a xwayland client, then over-ride the PID. */
            wlr_surface *wlr_surface = mView->get_wlr_surface();
#if WF_HAS_XWAYLAND

            if ( auto xwayland_surface = wlr_xwayland_surface_try_from_wlr_surface( wlr_surface ) ) {
                pid = xwayland_surface->pid;
            }

#endif

            /** Send the info */
            wayfire_toplevel_v1_process_info( mHandle, pid, uid, gid );
        }
    );

    toplevel_close_request.set_callback(
        [ & ] ( auto ) {
            mView->close();
        }
    );

    toplevel_kill_request.set_callback(
        [ & ] ( auto ) {
            pid_t pid = 0;
            uid_t uid = 0;
            gid_t gid = 0;

            wl_client_get_credentials( mView->get_client(), &pid, &uid, &gid );

            kill( pid, SIGKILL );
        }
    );
}
